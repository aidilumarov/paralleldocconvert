﻿using System;
using System.Linq;
using Plossum.CommandLine;

namespace ParallelDocConvert
{
    class Program
    {
        public const int LAUNCH_PROGRAM = 1;
        public static string InputDirectory;
        public static string OutputDirectory;
        public static int ConversionMethod;

        static void Main(string[] args)
        {
            int option = ProcessCommandLineOptions(args);

            //If ProcessCommandLineOptions return 1,
            //then we are good to go, in which case
            //we launch the conversion
            if (option == LAUNCH_PROGRAM)
            {
                Console.WriteLine("\nWorking...");
                try
                {
                    StartConvert(InputDirectory, OutputDirectory);
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    Console.WriteLine("You need to install Microsoft Office for this option");
                    return;
                }
                
                Console.WriteLine("\nDone.");
            }
        }

        //Process command-line arguments
        //Returns 1 if all checks are passed
        static int ProcessCommandLineOptions(string[] args)
        {
            CommandLineOptions options = new CommandLineOptions();
            CommandLineParser parser = new CommandLineParser(options);
            parser.Parse();
            Console.WriteLine(parser.UsageInfo.GetHeaderAsString(78));

            //If the user invoked -Help or gave no arguments
            if (options.H || args.Length == 0)
            {
                Console.WriteLine(parser.UsageInfo.GetOptionsAsString(78));
                return 0;
            }
            
            //If there are errors, -1 is returned, in which case Main() does nothing
            else if (parser.HasErrors)
            {
                Console.WriteLine(parser.UsageInfo.GetErrorsAsString(78));
                return -1;
            }
            
            //If everything is good to go, return 1
            else
            {
                InputDirectory = options.I;
                OutputDirectory = options.O;
                ConversionMethod = options.M;
                return 1;
            }
        }

        //Method to launch conversion
        static void StartConvert(string InputDirectory, string OutputDirectory)
        {
            ParallelPdfConversion convert = new ParallelPdfConversion();
            string[] filesList = ParallelDirectoryManipulation.ShowFiles(InputDirectory, "*.doc");

            if (filesList.Count() == 0)
            {
                Console.WriteLine("No files to convert in {0}", InputDirectory);
                return;
            }

            convert.ParallellizeConversion(filesList, ConversionMethod);
            ParallelDirectoryManipulation.MoveFiles(InputDirectory, OutputDirectory, "*.pdf");

        }
    }
}
