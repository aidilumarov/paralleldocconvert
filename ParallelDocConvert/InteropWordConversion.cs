﻿using System;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace ParallelDocConvert
{
    static class InteropWordConversion
    {
        /**
         * Accepts a file path and converts the file to PDF and Interop.Word.Application object
         * @param file - the full path of a file
         * @param app - Interop.Word.Application object
        **/
        public static void ConvertToPdf(string file)
        {
            Application app = new Application();
            if (app.Documents != null)
            {
                var convertFile = app.Documents.Open(file);
                if (convertFile != null)
                {
                    convertFile.ExportAsFixedFormat(file.Replace(Path.GetExtension(file), ".pdf"),
                    WdExportFormat.wdExportFormatPDF);
                    convertFile.Close();
                }
                app.Quit();
            }
        }
    }
}
