﻿using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using System.IO;


namespace ParallelDocConvert
{
    public static class SyncfusionWordConversion
    {
        /**
      * Accepts a file path and converts the file to PDF and Syncfusion.DocIo.DLS.WordDocument object, 
      * Syncfusion.DocToPDFConverter.DocToPDFConverter object, Syncfusion.Pdf.PdfDocument object
      * @param file - the full path of a file
     **/
        public static void ConvertToPdf(string file)
        {
            WordDocument wordDoc = new WordDocument(file, FormatType.Docx);
            DocToPDFConverter convertFile = new DocToPDFConverter();
            PdfDocument pdfDocument = convertFile.ConvertToPDF(wordDoc);
            convertFile.Dispose();
            wordDoc.Close();
            pdfDocument.Save(file.Replace(Path.GetExtension(file), ".pdf"));
            pdfDocument.Close(true);
        }
    }
}
