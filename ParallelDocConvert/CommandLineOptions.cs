﻿using System;
using Plossum.CommandLine;
using System.IO;

namespace ParallelDocConvert
{
    [CommandLineManager(ApplicationName = "\nParallel .DOC to .PDF Converter",
        Copyright = "Free Software by Aidil Umarov and Begimay Turdueva")]
    class CommandLineOptions
    {
        [CommandLineOption(Description = "Displays this help text")]
        public bool H = false;

        [CommandLineOption(Description = "Specify the directory for .DOC files", MinOccurs = 1, MaxOccurs = 1)]
        public string I
        {
            get { return mInputDirectory; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new InvalidOptionValueException(
                        "You must specify the directory with the files you wish to convert", false);
                else if (!Directory.Exists(value))
                    throw new DirectoryNotFoundException(
                        "Directory is not valid or does not exist");
                else mInputDirectory = value;
            }
        }

        [CommandLineOption(Description = "Specify the output directory for .PDF files", MinOccurs = 1, MaxOccurs = 1)]
        public string O
        {
            get { return mOutputDirectory; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new InvalidOptionValueException(
                        "You must specify the output directory for .PDF files", false);
                else if (!Directory.Exists(value))
                    throw new DirectoryNotFoundException(
                        "Directory is not valid or does not exist");
                else mOutputDirectory = value;
            }

        }

        [CommandLineOption(Description = "Specify the method of conversion: 1 for Interop.Word, 2 for Syncfusion",
            MinOccurs = 1, MaxOccurs = 1)]
        public int M
        {
            get { return mConversionMethod; }
            set
            {
                if (String.IsNullOrEmpty(value.ToString()))
                    throw new InvalidOptionValueException(
                        "You must specify the conversion method", false);
                
                else if (value < 1 || value > 2) throw new InvalidOptionValueException(
                    "You must specify the method, 1 or 2", false);
                
                mConversionMethod = value;
            }
        }

    private string mInputDirectory;
    private string mOutputDirectory;
    private int mConversionMethod;

    }
}

