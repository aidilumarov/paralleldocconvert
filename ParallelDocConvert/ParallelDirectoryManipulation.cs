﻿using System.Threading.Tasks;
using System.IO;

namespace ParallelDocConvert
{
    static class ParallelDirectoryManipulation
    {
        /**
         * The method gets all files from current directory and returns the string array, which contains the path of these files
         * @param folderPath - a string value, which describes the definite directory 
         * @param nameExtension - string vaue, which describes the required extension 
         **/
        public static string[] ShowFiles(string folderPath, string nameExtension)
        {
            return Directory.GetFiles(folderPath, nameExtension);
        }

        /**
         * Moves items from one directory to another
         * @param folderPath - a string value, which indicates the current directory
         * @param destinationPath - a string value, which indicates destination directory
         * @param nameExtension - which file types to move
         **/
        public static void MoveFiles(string folderPath, string destinationPath, string nameExtension)
        {
            if (folderPath.Equals(destinationPath)) return;
            var files = new DirectoryInfo(folderPath).GetFiles(nameExtension);
            Parallel.ForEach<FileInfo>(files, (file) =>
            {
                file.MoveTo(destinationPath + "\\" + file.Name);
            });
        }
    }
}
