﻿using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace ParallelDocConvert
{
    class ParallelPdfConversion
    { 
        /** 
         * The method accepts @param and converts to PDF file with a given conversion method
         * @param allFiles - the string array, which contains files paths  
         **/
        public void ParallellizeConversion(string[] allFiles, int conversionMethod)
        {
            if (conversionMethod == (int) ConversionMethods.InteropWord)
            {
                Parallel.ForEach<string>(allFiles, (file) =>
                {
                    InteropWordConversion.ConvertToPdf(file);
                });
            }

            else if (conversionMethod == (int) ConversionMethods.Syncfusion)
            {
                Parallel.ForEach<string>(allFiles, (file) =>
                {
                    SyncfusionWordConversion.ConvertToPdf(file);
                });
            }
            
        }

    }
}
